package com.tsystems.javaschool.tasks.calculator;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        return parser(statement);
    }

    private String formatResult(double d) {
        int intermediary = (int)(d*10000.0);
        double divider = 10000.0;
        while(intermediary%10==0&&divider>1) {
            intermediary = intermediary/10;
            divider = divider/10;
        }
        if(divider==1) return Integer.toString(intermediary);
        Double result = (double)intermediary/divider;
        return Double.toString(result);
    }

    /*
    Parser (Uses Shunting-yard Dijkstra Algorithm, O(n))
    We check if the lexer produced us a non-null result.
    If lexer is null, it means that lexicographically our string is not valid and should be discarded.
    Otherwise, we start to parse values and operators onto the separate stacks - values stack and operator stack.
    If we encounter left parenthesis, we push it on
    If we encounter right parenthesis:
        while(top of the operator stack is not left parenthesis)
            pop operator
            pop two values
            execute operator on values
            push result to valueStack
        pop left parenthesis
    If we encounter operators:
        While(stack is not empty and the top element has the same or greater precedence)
            Pop operator from the stack
            Pop two values from value stack
            Apply operator to values
            Push result to valueStack
         push operator on the operator stack
     Once we ran through, while operator stack is not empty, continue to correctly apply the operators, until operator stack is empty

     If, at any point the right (first) of the two values is 0 and the operator is divide, we return null.
     */

    private String parser(String statement) {
        if(lexer(statement)==null) return null;
        Stack<Character> operatorStack = new Stack<>();
        Stack<Double> valuesStack = new Stack<>();
        StringBuilder valueBuilder = new StringBuilder();
        int currentIndex = 0;
        while(currentIndex<statement.length()) {
            Character ch = statement.charAt(currentIndex);
            if(isNumeric(ch)||ch.equals('.')) valueBuilder.append(ch);
            else{
                if(valueBuilder.length()>0) {
                    valuesStack.push(Double.parseDouble(valueBuilder.toString()));
                    valueBuilder = new StringBuilder();
                }
            }
            if(valueBuilder.length()>0&&currentIndex==statement.length()-1) {
                valuesStack.push(Double.parseDouble(valueBuilder.toString()));
                valueBuilder = new StringBuilder();
            }
            if(ch.equals('(')) operatorStack.push(ch);
            if(ch.equals(')')) {
                while(operatorStack.peek()!='(') {
                    Double result = stackOp(valuesStack, operatorStack);
                    if(result==null) return null;
                    valuesStack.push(result);
                }
                operatorStack.pop();
            }
            if(isOperator(ch)) {
                while(!operatorStack.empty()&& (getPrecedence(ch)<=getPrecedence(operatorStack.peek()))) {
                    Double result = stackOp(valuesStack, operatorStack);
                    if(result==null) return null;
                    valuesStack.push(result);
                }
                operatorStack.push(ch);
            }
            currentIndex++;
        }
        while(!operatorStack.empty()) {
            Double result = stackOp(valuesStack, operatorStack);
            if(result==null) return null;
            valuesStack.push(result);
        }
        return formatResult(valuesStack.pop());
    }

    private int getPrecedence(char ch) {
        if(ch=='/'||ch=='*') return 2;
        if(ch=='(') return 0;
        else return 1;
    }

    private Double stackOp(Stack<Double> valuesStack, Stack<Character> operatorStack) {
        if(valuesStack.size()<2) return null;
        Double rightValue = valuesStack.pop();
        Double leftValue = valuesStack.pop();
        Character operator = operatorStack.pop();
        Double result = operate(leftValue,rightValue,operator);
        if(result==null) return null;
        return result;
    }

    private Double operate(double d1, double d2, Character operator) {
        if(operator.equals('+')) return add(d1, d2);
        if(operator.equals('-')) return subtract(d1,d2);
        if(operator.equals('*')) return multiply(d1,d2);
        if(operator.equals('/')) return divide(d1,d2);
        return null;
    }

    private Double add(double d1, double d2) {
        return d1+d2;
    }

    private Double subtract(double d1, double d2) {
        return d1-d2;
    }

    private Double multiply(double d1, double d2) {
        return d1*d2;
    }

    private Double divide(double d1, double d2) {
        if(d2==0) return null;
        return d1/d2;
    }


    /*
    Lexer
    Done pretty simply through linear run through the string:
    While we have numerics or a decimal, we keep the decimal marker.
    If we encounter decimal while marker is true, sequence is not valid. (eg 123.4.5)
    If we encounter an operator, we apply operator marker
    If we encounter an operator while operator marker is up, sequence is not valid (eg 1++5)
    If we encounter numeric and operator marker is true, we remove decimal marker and operator marker (this will only occur if and only if we have one operator and we start a new number)
    If we encounter a left parenthesis, we advance the parenthesis counter by one.
    If we encounter a right parenthesis and parenthesis counter is 0, we return null, otherwise decrease parenthesis counter by one
    If we ran through the string and parenthesis counter is not 0, we return null
    If string is empty, return null
     */

    private String lexer(String statement) {
        if(statement==null||statement.length()==0) return null;
        int currentIndex = 0;
        boolean decimalMarker = true;
        boolean operatorMarker = true;
        int parenthesisCount = 0;
        while(currentIndex<statement.length()) {
            Character ch  = statement.charAt(currentIndex);
            if(!isValid(ch, decimalMarker, operatorMarker, parenthesisCount)) return null;
            if(isNumeric(ch)&&operatorMarker) {decimalMarker = false; operatorMarker = false;}
            if(ch.equals('.')) decimalMarker = true;
            if(isOperator(ch)) operatorMarker = true;
            if(ch.equals('(')) parenthesisCount++;
            if(ch.equals(')')) parenthesisCount--;
            currentIndex++;
        }
        if(parenthesisCount>0) return null;
        return statement;
    }

    private boolean isValid(Character ch, boolean decimalMarker, boolean operatorMarker, int parenthesisCount) {
        if(!isValidCharacter(ch)) return false;
        if(ch.equals(')')&&parenthesisCount==0) return false;
        if(isOperator(ch)&&operatorMarker) return false;
        return !(decimalMarker&&ch.equals('.'));
    }

    private boolean isNumeric(Character ch) {
        return Character.isDigit(ch);
    }

    private boolean isOperator(Character ch) {
        return ch.equals('-')|| ch.equals('+')||ch.equals('*')||ch.equals('/');
    }

    private boolean isValidCharacter(Character ch) {
        return isNumeric(ch)||isOperator(ch)||ch.equals('.')||ch.equals('(')||ch.equals(')');
    }

}
