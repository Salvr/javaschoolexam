package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    /*
    Basic greedy algorithm.
    We only need to see if the next element of X is in Y
    If we found it, we iterate through X one step.
    If at the end we iterated through all elements of X, we return true
    Otherwise, we return false.
     */
    public boolean find(List x, List y) {
        if(x==null || y == null) throw new IllegalArgumentException();
        if(x.size()>y.size()) return false;
        int indexX = 0;
        int indexY = 0;
        while(indexX<x.size()&&indexY<y.size()) {
            if(x.get(indexX).equals(y.get(indexY))) {
                indexX++;
            }
            indexY++;
        }
        return indexX==x.size();
    }
}
