package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    /*
    Idea:
    Pyramids are sequences starting with 1 element at the top row and adding +1 for each row after that
    Therefore, sizes of the pyramid are 1, 3, 6, 10, 15...etc or Triangular Numbers
    First we check if the number is triangular.
    If it is not, we throw an exception
    If it is, we check all the elements if any of them are null
    If they are, we throw an exception.
    Then we sort all the elements.
    The row length increases with each pyramid height by 2.
    Starting at 1 when pyramid is 1 size
    Then 3, 5, 7, 9... etc
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[] row = new int[1]; // easiest way to pass by reference without creating a new class
        if(!isTriangular(inputNumbers.size(), row)) throw new CannotBuildPyramidException("Cannot build a pyramid: incorrect element amount");
        for(int i = 0; i<inputNumbers.size(); i++) {
            if(inputNumbers.get(i)==null) throw new CannotBuildPyramidException("Cannot build a pyramid: null elements are found");
        }
        Collections.sort(inputNumbers);
        int columns = row[0]*2 - 1;
        int[][] result = new int[row[0]][columns];
        build(result, inputNumbers);
        return result;
    }

    // Quadratic equation to check the triangularity of the number
    public boolean isTriangular(int number, int[] rows) {
        int c = (-2)*number;
        int b = 1;
        int a = 1;
        int determinant = (b*b)-(4*a*c);
        if(determinant<0) return false;
        float firstRoot = (-b + (float)Math.sqrt(determinant))/(2*a);
        float secondRoot = (-b - (float)Math.sqrt(determinant))/(2*a);
        if(firstRoot>0 && Math.floor(firstRoot)==firstRoot){ rows[0] = (int)Math.floor(firstRoot); return true;}
        if(secondRoot>0 && Math.floor(secondRoot)==secondRoot) {rows[0] = (int)Math.floor(secondRoot); return true;}
        return false;
    }

    public void build(int[][] pyramid, List<Integer> numbers) {
        int numbersToUseOnRow = 1;
        int index = 0;
        int offset = pyramid.length-1; // amount of rows - 1;
        for(int i = 0; i<pyramid.length; i++) {
            int numbersLeftToUse = numbersToUseOnRow;
            int j = offset;
            while (numbersLeftToUse>0) {
                pyramid[i][j]= numbers.get(index);
                j+=2;
                index++;
                numbersLeftToUse--;
            }
            numbersToUseOnRow++;
            offset--;
        }
    }


}
